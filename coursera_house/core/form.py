from django import forms


class ControllerForm(forms.Form):

    bedroom_target_temperature = forms.IntegerField(min_value=16, max_value=50, label='Желаемая температура в спальне')
    hot_water_target_temperature = forms.IntegerField(min_value=24, max_value=90, label='Желаемая температура воды в бойлере')
    bedroom_light = forms.BooleanField(required=False, label='Свет в спальне')
    bathroom_light = forms.BooleanField(required=False, label='Свет в ванной')

    # mapping for controllers in API and their name and values representation in UI. Since it is not homogeneous but
    # known in advance, use one key for controller name and others for value. Key 'name' always will represent
    # controller name, value will always be found within other keys
    labels = {
        'air_conditioner': {
            'name': 'Кондиционер',
            True: 'Вкл.',
            False: 'Выкл.'
        },
        'bedroom_temperature': {
            'name': 'Температура в спальне'
        },
        'bedroom_light': {
            'name': 'Лампа в спальне',
            True: 'Вкл.',
            False: 'Выкл.'
        },
        'smoke_detector': {
            'name': 'Датчик задымления на потолке',
            True: 'Задымление!',
            False: 'Нет'
        },
        'bedroom_presence': {
            'name': 'Датчик присутствия в спальне',
            True: 'Есть присутствие',
            False: 'Никого нет'
        },
        'bedroom_motion': {
            'name': 'Датчик движения в спальне',
            True: 'Есть движение',
            False: 'Никого нет'
        },
        'curtains': {
            'name': 'Занавески',
            'open': 'Открыты',
            'close': 'Закрыты',
            'slightly_open': 'Ручное управление'
        },
        'outdoor_light': {
            'name': 'Датчик освещенности за окном'
        },
        'boiler': {
            'name': 'Бойлер',
            True: 'Вкл.',
            False: 'Выкл.'
        },
        'boiler_temperature': {
            'name': 'Температура горячей воды в бойлере'
        },
        'cold_water': {
            'name': 'Входной кран холодной воды',
            True: 'Открыт',
            False: 'Закрыт'
        },
        'hot_water': {
            'name': 'Входной кран горячей воды',
            True: 'Открыт',
            False: 'Закрыт'
        },
        'bathroom_light': {
            'name': 'Лампа в ванной',
            True: 'Вкл.',
            False: 'Выкл.'
        },
        'bathroom_presence': {
            'name': 'Датчик присутствия в ванной',
            True: 'Есть присутствие',
            False: 'Никого нет'
        },
        'bathroom_motion': {
            'name': 'Датчик движения в ванной',
            True: 'Есть движение',
            False: 'Никого нет'
        },
        'leak_detector': {
            'name': 'Датчик протечки воды',
            True: 'Протечка!',
            False: 'Сухо'
        },
        'washing_machine': {
            'name': 'Стиральная машина',
            'on': 'Вкл.',
            'off': 'Выкл.',
            'broken': 'Сломана'
        }
    }
