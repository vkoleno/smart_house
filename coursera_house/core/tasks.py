from __future__ import absolute_import, unicode_literals
import requests
from ..settings import SMART_HOME_ACCESS_TOKEN, SMART_HOME_API_URL, EMAIL_RECEPIENT
from celery import task
from coursera_house.core.models import Setting
from django.core.mail import send_mail

auth_header = {'Authorization': 'Bearer {}'.format(SMART_HOME_ACCESS_TOKEN)}

# smart_home_manager will be run periodically, want to keep variable values between function runs
cold_water_lock = False
hot_water_lock = False
curtains_lock = False
smoke_lock = False


@task()
def smart_home_manager():
    payload = {}
    global cold_water_lock, hot_water_lock, curtains_lock, smoke_lock
    get_controllers_response = requests.get(SMART_HOME_API_URL, headers=auth_header)
    data = get_controllers_response.json()['data']
    api_values = {item['name']: item['value'] for item in data}
    stored_values = {param.controller_name: param.value for param in Setting.objects.all()}

    # leak handle
    if api_values['leak_detector']:
        payload['cold_water'] = False
        payload['hot_water'] = False
        cold_water_lock = True
        hot_water_lock = True
        send_mail(
            'Water leak detected!',
            'Our system reporting water leakage. Please check asap',
            'no-reply@smarthome.com',
            [EMAIL_RECEPIENT]
        )
    if not api_values['leak_detector']:
        hot_water_lock = False
        cold_water_lock = False

    # boiler and washing machine
    if not api_values['cold_water'] or cold_water_lock:
        payload['boiler'] = False
        payload['washing_machine'] = 'off'

    # water heat
    if api_values['cold_water'] \
            and not cold_water_lock \
            and not smoke_lock \
            and api_values['boiler_temperature'] <= stored_values['hot_water_target_temperature'] * 0.9:
        payload['boiler'] = True
    if api_values['cold_water'] \
            and not cold_water_lock \
            and api_values['boiler_temperature'] >= stored_values['hot_water_target_temperature'] * 1.1:
        payload['boiler'] = False

    # curtains
    if api_values['curtains'] == 'slightly_open':
        curtains_lock = True
    elif api_values['curtains'] in ('open', 'close'):
        curtains_lock = False
    if api_values['outdoor_light'] <= 50 and not api_values['bedroom_light'] and not curtains_lock:
        payload['curtains'] = 'open'
    elif api_values['outdoor_light'] >= 50 or api_values['bedroom_light'] and not curtains_lock:
        payload['curtains'] = 'close'

    # smoke detector
    if api_values['smoke_detector']:
        smoke_lock = True
        payload['air_conditioner'] = False
        payload['bedroom_light'] = False
        payload['bathroom_light'] = False
        payload['boiler'] = False
        payload['washing_machine'] = 'off'
    else:
        smoke_lock = False

    # air conditioning
    if api_values['bedroom_temperature'] >= stored_values['bedroom_target_temperature'] * 1.1 and not smoke_lock:
        payload['air_conditioner'] = True
    elif api_values['bedroom_temperature'] <= stored_values['bedroom_target_temperature'] * 0.9:
        payload['air_conditioner'] = False

    # lights
    if stored_values['bedroom_light'] and not smoke_lock:
        payload['bedroom_light'] = True
    else:
        payload['bedroom_light'] = False
    if stored_values['bathroom_light'] and not smoke_lock:
        payload['bathroom_light'] = True
    else:
        payload['bathroom_light'] = False

    # format of request body is {'controllers': [{'name': 'value'}...]}
    controllers_payload = [{'name': controller, 'value': value} for controller, value in payload.items()]
    update_response = requests.post(SMART_HOME_API_URL, json={'controllers': controllers_payload}, headers=auth_header)
    current_state = {item['name']: item['value'] for item in update_response.json()['data']}
    if update_response.status_code is not 200 or get_controllers_response.status_code is not 200:
        current_state['smart_home_bad_api_status'] = update_response.status_code

    return current_state
