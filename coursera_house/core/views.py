from __future__ import absolute_import, unicode_literals
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.http import HttpResponse
from .models import Setting
from .form import ControllerForm
from .tasks import smart_home_manager


class ControllerView(FormView):
    # By requirements demanded that if smart home api responded with error, 502 http code should be returned. Having
    # smart_home_bad_api_status variable to check for it in context from smart_home_manager (since it handles smart
    # home api response
    smart_home_bad_api_status = 'smart_home_bad_api_status'
    form_class = ControllerForm
    template_name = 'core/control.html'
    success_url = reverse_lazy('form')

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if self.smart_home_bad_api_status not in context:
            return self.render_to_response(context)
        else:
            return HttpResponse(status=502)

    # overriding get_context_data to check if smart home api failed
    def get_context_data(self, **kwargs):
        result = smart_home_manager.delay()
        current_state = result.get()
        context = super().get_context_data()
        labels = ControllerForm.labels
        if self.smart_home_bad_api_status in current_state:
            context[self.smart_home_bad_api_status] = current_state[self.smart_home_bad_api_status]
            del current_state[self.smart_home_bad_api_status]
        context['data'] = {labels[controller]['name']: labels[controller].get(value, value)
                           for controller, value in current_state.items()}
        return context

    def get_initial(self):
        stored_settings = Setting.objects.all()
        initial_settings = {}
        # from UI checkboxes for bathroom and bedroom returns 0 and 1 as false/true. Values for temperatures can not be
        # at these values according to requirements so if setting value is 0/1 considering that it is false/true
        for setting in stored_settings:
            if setting.value == 1:
                initial_settings[setting.controller_name] = True
            elif setting.value == 0:
                initial_settings[setting.controller_name] = False
            else:
                initial_settings[setting.controller_name] = setting.value
        return initial_settings

    def post(self, request, *args, **kwargs):
        form = ControllerForm(request.POST)
        if form.is_valid():
            settings = form.cleaned_data
            for controller, value in settings.items():
                query_result = Setting.objects.filter(controller_name=controller)
                # Space for adding new settings - if it is not exists, create it
                if query_result.exists():
                    query_result.update(value=value)
                else:
                    new_setting = Setting(controller_name=controller,
                                          value=value,
                                          label=ControllerForm.labels[controller])
                    new_setting.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
