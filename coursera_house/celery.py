from __future__ import absolute_import, unicode_literals
import os
import django
from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'coursera_house.settings')
django.setup()


app = Celery('coursera_house')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
print(app.conf.result_backend, app.conf.broker_url)

# smart home manager uses celery task decorator so celery should be instantiated before importing it
from coursera_house.core.tasks import smart_home_manager


@app.on_after_finalize.connect()
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(5, smart_home_manager.s(), name='Check Smart Home')
